# CHANGELOG
### 16 Dec 2022
- Back at it after a long break!  Going to finish the PSU schematic page for the Apple II, which will finish the Apple II schematic altogether.

### 21 Dec 2022
- Finished the Apple II schematic (finally!).
- Due to the current popularity in the community, I'm going to do a schematic of the NABU computer next.
