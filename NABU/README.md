# NABU
These are the schematics for the NABU PC, a Z80 based 8-bit computer.  The NABU PC was created in 1983 and was used to connect to the NABU Network, a cable-TV based early precursor to the World Wide Web that was available in Ottowa, Canada from 1982 to 1985.

A large number of the NABU PCs were recently posted on Ebay and the NABU hardware has become quite popular in the retro computing community.

The source document for this schematic replica is the "NABU Consolidated Documentation v1.0a" PDF.  This is a document compiled by user starkscon on Github, containing all the documentation he was able to find while exploring is own NABU.

## Notes on the schematic
- For whatever reason, the original schematic contains no part designators for any passives.  The only designators are for the ICs and I will label them thusly in the schematic.

## Links
- Check out starkscon on Github - [https://github.com/starkscon/nabu-z80](https://github.com/starkscon/nabu-z80)
- Adrian's Digital Basement video exploring the NABU - [https://www.youtube.com/watch?v=HLYjZoShjy0](https://www.youtube.com/watch?v=HLYjZoShjy0)
